
  
# Regov - WeIDY

A basic Web3 Identity solution which is intended to demonstrate the workflow of Verifiable Credentials and its inhernet workflows on a basic level.

  
  

## Problem Context

City Government **(Issuer)** want to issue **Identities** to citizens **(Holder)**.

Later on, service providers **(Verifier)** can verify/onboard customers via their **Identities**

  

## Solution Overview

  

Citizens can download an application called Agent & run it on their local machine.

Agent application runs locally without server connection, only connects to the ledger (Node pool) & other agent apps (P2P).

For simplicity & production standard adaptability, Agent application holds only 1 session at a time. For demo purpose, we will run multiple Agent applications to simulate Issuer, Holder, Verifier.

  

The solution applies Permissioned Public Blockchain & Decentralized Identity mechanism using Hyperledger Indy's toolkits & frameworks:

1. **Node Pool/ Ledger**: Indy Ledger, in this case, **BCovrin Test Indy Ledger** is used (http://test.bcovrin.vonx.io/)

2. **Agent**: Hyperledger Aries + Typescript (NestJS)

  

## Installation

  1. We need Docker & Docker compose installed, we can follow this guide to install these tools: 
  https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04
  
  2. Move to the ***section2*** directory 
  3. Run:  `docker compose up`
  4. To check out whether Agent apps running properly, call to the corresponding hosts 
-	***Issuer***: `localhost:8080`, `
-	***Holder***: `localhost:8081`
-	***Verifier***: `localhost:8082`
  
## Workflow

#### Issuing Identity
##### 1. Both Issuer, Holder, Verifier login to their own applications
##### 1.1. If credential schema & definittion are not created, Issuer must create new schema & credential definition
##### 2. Issuer invite Holder for connection (Out-of-band)
##### 3. Holder accept connecting invitation
##### 4. Issuer create credential offer & send it to Holder via the connection
##### 5. Holder accept & store the credential

  

#### Verifying Identity
##### 1. Verifier invite Holder for connection (Out-of-band)
##### 2. Holder accept connecting invitation
##### 3. Verifier create proof request & send it to Holder via the connection
##### 4. Holder build proof from credentials & send the proof to Verifier via the connection
##### 5. Verifier can views & verify the proof

  

## APIs

Please checkout endpoint **/api** for API docs with Swagger
