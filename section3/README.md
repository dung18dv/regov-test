  
# Section 3 

## Assumption
-  **Identity** & **Payment** are also ***non-revocable*** decentralized credentials 
	- **Identity** issued by Governments 
	- **Payment** issued by Payment channels

- MTCT's HQ control the buying & selling price of assets


## Solution Overview
The solution applies Permissioned Public Blockchain & Decentralized Identity mechanism using Hyperledger Indy & Aries.

### Presiquities
- MTCT has role Trust Anchor on Indy Network.
- MTCT's HQ, Branches & Customer Portal agents use different wallets but having same ***publicDid***.
- MTCT define diferent **Ownership** (revocable) credential schemas for digital assets (eg: Stock, Forex, Crypto, NFT...). 
- MTCT define **Order** (non-revocable) credential schema, with fields: `customer, order_type(SELL | BUY), items[{asset_info, price}]`.
	 
### Flow designs
Check out ***onboarding*** & ***buy&sell*** diagrams.